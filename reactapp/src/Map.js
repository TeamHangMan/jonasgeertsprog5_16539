import React from 'react'
import { Map as LeafletMap, TileLayer, Marker, Popup } from 'react-leaflet';
import dataForMarkers from './data/LijstAntwerpen.json';

//Ik heb de link van David gebruikt voor de basis map te maken, de uitbreidingen heb ik zelf gemaakt/gezocht

// bron:  https://medium.com/@eugenebelkovich/map-for-react-apps-with-leaflet-365f9df82d55

// npm install leaflet react-leaflet
// de CSS toevoegen aan public/index.html: <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/leaflet.css">
// in css de class .leaflet-container instellen (hoogte en breedte)

class Map extends React.Component {
  
  
  
  //Dit opent de popup automatisch
  openPopup (marker) {
    if (marker && marker.leafletElement) {
      window.setTimeout(() => {
       // this.map.closePopup();
        marker.leafletElement.openPopup();
         
      })
    }
  }
  
  
 
    list = dataForMarkers.Curiosity;

    row = this.list.map((item) => {
        
        return (
            <Marker position={[item.latitude, item.longitude]}>
                <Popup  >
             
            {item.name}
            
            <br/>
            
            <button class="detailButton" onClick={() => this.props.action('Detail', item) }>Detail</button>
             
          </Popup>
            </Marker>
        );
    });
  render() {
      let url = `images/small/${this.props.data.image}`;
        let alt = ` foto van ${this.props.data.name}`;
    return (
      <LeafletMap
        center={[this.props.data.latitude, this.props.data.longitude]}
        zoom={19}
        maxZoom={20}
        attributionControl={true}
        zoomControl={true}
        doubleClickZoom={true}
        scrollWheelZoom={true}
        dragging={true}
        animate={true}
        easeLinearity={0.35}
      >
        <TileLayer
          url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
        />
        {this.row}
        <Marker  position={[this.props.data.latitude, this.props.data.longitude]}  ref={this.openPopup}>
          <Popup>
            <img src={url} alt={alt} className="picture-popup"/>
            <br/>
            {this.props.data.name}
            {this.openPopup()}
            
            
          </Popup>
        </Marker>
        
      </LeafletMap>
    );
  }
}

export default Map;