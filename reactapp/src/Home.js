import React, { Component } from 'react';
import Slider from './Slider';

class Home extends Component {
    render() {
        return (
            <div className="App">
            <header className="App-header">
              <p>
                <h1>
                Welkom in Antwerpen
                </h1>
                </p>
                <p className="sliderP">
               <Slider />
              </p>
              <button onClick={() => this.props.action('List')}>Lijst</button>
              
              
            </header>
          </div>
        );
    }
}

export default Home;