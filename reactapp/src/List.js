import React, { Component } from 'react';
import data from './data/LijstAntwerpen.json'


class List extends Component {
  list = data.Curiosity;
    
    row = this.list.map((item) => {
        
        let url = `images/small/${item.image}`;
        let alt = ` foto van ${item.name}`;
        
        return (
            <tr>
                <td><img src={url} alt={alt} /></td>
                <td>{item.name}</td>
               
                <td> <button onClick={() => this.props.action('Detail', item)}>Detail</button></td>
                 
            </tr>
        );
    });
   

    render() {
        //console.log(this.state.commentList);
       // console.log(data.Curiosity);
        return (
            <table>
                {this.row}
            </table>
        );
    }
}

export default List;