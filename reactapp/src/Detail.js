import React, {Component} from 'react';
import Map from './Map';

class Detail extends Component {
   handleClick = this.props.action;
   
  handleSubmit(event) {
    event.preventDefault();
    const data = new FormData(event.target);
    const url = `https://tempelers-j-tjeremy.c9users.io/mmt-php-api/public/curiositycomment/readallwhere?CuriosityId=${this.props.data.id}`;
    fetch(url, {
      method: 'POST',
      body: data,
    });
  }
  
    
     commentList = null;
    state = {'commentList' : this.commentList};


commentListUI = function() {
       this.commentList = this.state.commentList;
       if(this.commentList === null) {
            this.commentList = ['Geen'];
        }
        let commentUI = this.commentList.map(item => {
            return (
                <p class="comment">
                    <strong>{item.UserName}</strong>: {item.Comment}
                </p>
            )
        });
        return commentUI;
    }

   



    

    // Code is invoked after the component is mounted/inserted into the DOM tree.
    componentDidMount() {
       
        const prepUrl = `https://tempelers-j-tjeremy.c9users.io/mmt-php-api/public/curiositycomment/readallwhere?CuriosityId=${this.props.data.id}`;
        const url = prepUrl;

        // alles over fetch:
        // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
        fetch(url, {
            credentials: 'omit'  
        })
            .then(result => {
                return result.json()
                })
            .then(result => {
                this.setState({
                    commentList: result
                })
            }).catch(function (result)
                {alert('Error: ' + result);} );
    }

    
    render() {
         let url = `images/small/${this.props.data.image}`;
        let alt = ` foto van ${this.props.data.name}`;
        return (
            <article>
               <div align="center">
               <button  onClick={() => this.props.action('List')}>Back To List</button>
               </div>
               
              
               <div class="detail" align="center">
                
                
                <h1>Detail {this.props.data.name} </h1>
                
                <p><img src={url} alt={alt} className="picture-detail"/></p>
                
                <div>Map to location: </div>
                <div><Map action={this.handleClick} data={this.props.data}/></div>
                <h3>Comments</h3>
                {this.componentDidMount()}
                {this.commentListUI()}
                
                
            </div>
             </article>
        )
    }
}
export default Detail;